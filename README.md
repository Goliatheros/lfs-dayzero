///////////////////////////////////////////////////////////////////////////////////////////////////////////////	
# GIT AND CONFIGURATION

# Linux From Scratch - dayzero

Linux from scratch creating OS with community

1. Creating stable OS with Debian multi (626mo) with this iso [HERE](https://gitlab.com/Goliatheros/lfs-dayzero/-/blob/main/os-development/debian-multi.iso)

    install debian os on vmware   ![vmware](images/vmware.jpg "vmware")                     

2. Create environment Gitlab for pushing code 

    ``curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash``

3. Clone environment Gitlab

    ``git clone git@example.com:project-name.git``  // to clone your environment for workplace.

4. Commit environment Gitlab

    ``git commit -m "add information for code"``

5. Finality of configuration Git and repository
![repertoire](images/repertoire.jpg "repertoire")

    *	/LFS BIBLE              (Listing and step for creating OS)
    *	/lfs-dayzero		    (OS development day zero)                                       
    *	/lfs-git		        (Linux from scratch dependancie and respository of development)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////	
# DEVELOPEMENT UNIX KERNEL 

--The kernel --
![kernel](diagramme du kernel.jpg "kernel")

1. Creating partition of development for LFS input usb keys

    ``fdisk -l`` and  ``fdisk /dev/sd*`` for * write the good number of your usbkey

    ``mkdir /media/USB`` and ``mount /dev/sdb* /media/USB`` and now your usbkey at the name of USB on your system

	Now the usbkey has mount on your system you can create a folder for create the final kernel.

2. Modify the kernel

	go on the folder /home/lfs/lfs-dayzero/ now you see them ![kernel](images/kernel.jpg "kernelfolder") 
	go on the folder "kernel" now you see them ![kernelfolder](images/onkernel.jpg "kernelfold")

3. Work on the kernel in usbkey

	now you write this command ``cp -r kernel/ /media/USB`` it will take some minutes be patient...
